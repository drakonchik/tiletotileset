/*****************************************************************************/
/*  Author        : Drakonchik                                               */
/*  Machine       : Personal Computer						                 */
/*  OS            : Ubuntu 20.04     						                 */
/*  Language      : GNU C++                                                  */
/*                                                                           */
/*  File Contents : MAIN FORM                                                */
/*  File Attribute: SOURCE                                                   */
/*  File Name     : mainwindow.cpp                                           */
/*  User Interface: mainwindow.ui                                            */
/*                                                                           */
/*****************************************************************************/

#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QPixmap>
#include <QVector>
#include <QFileDialog>
#include <QImage>
#include <QMessageBox>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QPainter>
#include <QPixmap>

QVector<QImage> ImageArray;

int TileCount;

int imageWidth;
int imageHeight;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    TileCount = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_AddItemsButton_clicked()
{
    QFileDialog dialog(this);
    dialog.setDirectory(QDir::homePath());
    dialog.setFileMode(QFileDialog::ExistingFiles);
    QStringList fileNames;
    if (dialog.exec())
    {
        fileNames = dialog.selectedFiles();
        for(int i = 0; i < fileNames.size(); i++)
        {
            QImage Imagy(fileNames.at(i));
            ImageArray.push_back(Imagy);
            ui->ImageListView->addItem(fileNames.at(i));
            TileCount++;
        }

    }



}

void MainWindow::on_actionAbout_TiletoTileset_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("Tile to Tileset is an Open Source project made for the creation of tilesets from individual Tiles.\nThis is a non-commercial project, if you paid for this product, you might have been scammed.\nPlease support the official release of this product and its author Drakonchik.\nPlease also suppoort the main project RPG Maker PS.");
    msgBox.exec();
}

void MainWindow::on_ConvertButton_clicked()
{
    CalculateImage();
    TileScene.clearSelection();
    TileScene.setSceneRect(QRectF(0,0,imageWidth,imageHeight));
    int i = 0;
    int j = 0;
    for(int z = 0; z < TileCount; z++)
    {

        if(i % ui->WidthTiles->value() == 0 && i != 0)
        {
            j++;
            i = 0;
        }

        QPixmap newPixmapItem = QPixmap::fromImage(ImageArray[z]);
        QGraphicsPixmapItem *pm = TileScene.addPixmap(newPixmapItem);
        pm->setPos(i*ui->TileWidth->value(), j*ui->TileHeight->value());
        i++;
    }

}

void MainWindow::CalculateImage()
{
    int width = ui->TileWidth->value();
    int height = ui->TileHeight->value();
    int tileWcount = ui->WidthTiles->value();

    // This part here rounds up the number
    int roundHeight = (TileCount - TileCount % tileWcount) / tileWcount;

    int addedValue = 0;

    if(TileCount % tileWcount > 0)
    {
        addedValue = ui->TileWidth->value();
    }
    else
    {
        addedValue = 0;
    }

    imageWidth = width * tileWcount;
    imageHeight = height * roundHeight + addedValue;

}


void MainWindow::on_SaveAsButton_clicked()
{
    QImage image(TileScene.sceneRect().size().toSize(), QImage::Format_ARGB32);  // Create the image with the exact size of the shrunk scene
    image.fill(Qt::transparent);                                              // Start all pixels transparent
    QPainter painter(&image);
    TileScene.render(&painter);
    QString name = ui->outputFileName->text();

    image.save(name);
}

void MainWindow::on_actionHow_To_Use_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("Idk... Good Luck!");
    msgBox.exec();
}

void MainWindow::on_actionAboutQT_triggered()
{
    QApplication::aboutQt();
}

void MainWindow::on_DeleteButton_clicked()
{
    TileScene.clear();
    ImageArray.clear();
    ui->ImageListView->clear();
    TileCount = 0;
}
