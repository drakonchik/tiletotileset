#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void CalculateImage();


private slots:
    void on_AddItemsButton_clicked();

    void on_actionAbout_TiletoTileset_triggered();

    void on_ConvertButton_clicked();

    
    void on_SaveAsButton_clicked();

    void on_actionHow_To_Use_triggered();

    void on_actionAboutQT_triggered();

    void on_DeleteButton_clicked();

private:
    Ui::MainWindow *ui;
    // TileMap and TileSet Graphics Scene Variables
    QGraphicsScene TileScene;
};
#endif // MAINWINDOW_H
